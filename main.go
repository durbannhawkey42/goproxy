package main

import (
	"context"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

func main() {
	// 创建反向代理的目标URL
	backendURL, err := url.Parse("http://localhost:8080/")
	if err != nil {
		log.Fatal(err)
	}

	// 创建反向代理服务器
	proxy := httputil.NewSingleHostReverseProxy(backendURL)

	// 创建HTTP处理函数，用于将请求转发到后端服务器并返回响应
	http.HandleFunc("/api/", func(w http.ResponseWriter, r *http.Request) {
		// 修改请求的主机为后端服务器的主机
		r.Host = backendURL.Host

		// 更新响应的Header中的地址，以便正确返回给前端服务器
		r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))

		// 设置超时时间为5秒
		timeout := time.Duration(5 * time.Second)
		ctx, cancel := context.WithTimeout(r.Context(), timeout)
		defer cancel()
		r.WithContext(ctx)

		// 重写url
		if strings.HasPrefix(r.URL.Path, "/api/") {
			r.URL.Path = strings.Replace(r.URL.Path, "/api/", "", 1)
		}
		// 转发请求到后端服务器并返回响应给前端服务器
		proxy.ServeHTTP(w, r)
	})

	// 启动前端服务器
	log.Println("Starting frontend server on localhost:5173")
	err = http.ListenAndServe(":5173", nil)
	if err != nil {
		log.Fatal(err)
	}
}
